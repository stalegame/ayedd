#include "graph.h"

GrafoTipo::GrafoTipo(int maxV){
   numVertices = 0;
   maxVertices = maxV;
   vertices = new int[maxV];
   aristas = new int*[maxV];

   for(int i = 0; i < maxV; i++)
     aristas[i] = new int[maxV];

   marcas = new bool[maxV];
}

GrafoTipo::~GrafoTipo(){
   delete [] vertices;

   for(int i = 0; i < maxVertices; i++)
      delete [] aristas[i];

   delete [] aristas;
   delete [] marcas;
}

void GrafoTipo::agregar_vertice(int ver){
   vertices[numVertices] = ver;

   for(int index = 0; index < numVertices; index++) {
     aristas[numVertices][index] = NULL_EDGE;
     aristas[index][numVertices] = NULL_EDGE;
   }

   numVertices++;
}

void GrafoTipo::agregar_arista(int v_inicio, int v_final, int peso){
   int fila, columna;

   fila = indice(vertices, v_inicio);
   columna = indice(vertices, v_final);
   aristas[fila][columna] = peso;
}

int GrafoTipo::calcular_peso(int v_inicio, int v_final){
   int fila, columna;

   fila = indice(vertices, v_inicio);
   columna = indice(vertices, v_final);

   return aristas[fila][columna];
}

int GrafoTipo::indice(int *vertices, int ver){
  for(int i = 0; i < numVertices; i++){
      if (vertices[i] == ver)
          return i;
  }

  return -1;
}

bool GrafoTipo::grafo_lleno(){
  if (numVertices == maxVertices)
      return true;

  return false;
}

bool GrafoTipo::grafo_vacio(){
  if (numVertices == 0)
      return true;

  return false;
}

int GrafoTipo::largo_grafo(){
  return numVertices;
}
