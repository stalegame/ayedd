#include <iostream>
#include "graph.h"

using namespace std;

int main() {
    GrafoTipo x(10);

    cout << "Vació? " << x.grafo_vacio() << " - " << x.largo_grafo() << endl;


    x.agregar_vertice(10);
    x.agregar_vertice(20);
    cout << "\nVació? " << x.grafo_vacio() << " - " << x.largo_grafo() << endl;
    x.agregar_arista(10, 20, 100);
    cout << "Peso: " << x.calcular_peso(10, 20) << endl;
    x.agregar_vertice(30);
    cout << "\nVació? " << x.grafo_vacio() << " - " << x.largo_grafo() << endl;
    x.agregar_arista(10, 30, 200);
    cout << "Peso: " << x.calcular_peso(10, 30) << endl;

}
