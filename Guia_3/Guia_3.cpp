#include<iostream>
#include<stdlib.h>

using namespace std;

/////
class Nodo{
    public:
        int matricula;
        float promedio;

        Nodo *izquierdo;
        Nodo *derecho;

        // constructor
        Nodo(int matricula, float prom):matricula(matricula), izquierdo(NULL), derecho(NULL), promedio(prom){}
};

class ArbolBinario {
    private:

        Nodo *insertar(Nodo *prev, int matricula, float prom){
            if(prev == NULL){
                return new Nodo(matricula, prom);
            }

            else{

                if(matricula >= prev->matricula){
                    prev->izquierdo = insertar(prev->izquierdo, matricula, prom);
                }else{
                    prev->derecho = insertar(prev->derecho, matricula, prom);
                }
            }
            return prev;
        }

    public:
        Nodo *raiz;
        ArbolBinario() :raiz(NULL) {}

        void Agregar(int valor, float prom){
            raiz = insertar(raiz, valor, prom);
        }

        void in_order(Nodo *actual){
            if (actual == NULL){
                return;
            }
            in_order(actual->izquierdo);
            cout << " " << actual->matricula;
            in_order(actual->derecho);
        }

        void pre_order(Nodo *actual){
            if (actual == NULL){
                return;
            }
            cout << " " << actual->matricula;
            pre_order(actual->izquierdo);
            pre_order(actual->derecho);
        }

        void post_order(Nodo *actual){
            if (actual == NULL){
                return;
            }
            post_order(actual->izquierdo);
            post_order(actual->derecho);
            cout << " " << actual->matricula;
        }

};

int main(){
    ArbolBinario tree;

    int a=0, b=0, matr;
    float pro;

    while(a!=5){
        cout << "<1> Ingresar nueva matricula y promedio \n";
        cout << "<2> Mostrar arbol de forma 'pre order' \n";
        cout << "<3> Mostrar arbol de forma 'In order' \n";
        cout << "<4> Mostrar arbol de forma 'post order' \n";
        cout << "<5> Salir de la aplicacion \n";
        cin >> a;
        cout << "\n";

        switch(a){
            case 1:
                cout << "Ingrese la matricula: ";
                cin >> matr;
                cout << "Ingrese el promedio: ";
                cin >> pro;
                cout << "\n";
                tree.Agregar(matr, pro);
                break;

            case 2:

                cout << "Arbol en pre-order\n";
                tree.pre_order(tree.raiz);
                cout <<"\n";
                break;

            case 3:

                cout << "Arbol en in-order\n";
                tree.in_order(tree.raiz);
                cout <<"\n";
                break;

            case 4:

                cout << "Arbol en post-order\n";
                tree.post_order(tree.raiz);
                cout <<"\n";
                break;

            case 5:
                cout << "Gracias por usar el codigo\n";
                return 0;
            default:
                break;
        }
    }
};
